//race condition
#include <stdio.h>
#include <pthread.h>
unsigned int sleep(unsigned int seconds);
int c;
void *c_inc(void *args)
{
 int x = c;
 x++;
 sleep(2);
 c = x;
}
void *c_dec(void *args)
{
 sleep(2);
 int x = c;
 x--;
 c = x;
}
int main()
{
 pthread_t inc;
 pthread_t dec;
 c = 1;
 pthread_create(&inc, NULL, c_inc, NULL);
 pthread_create(&dec, NULL, c_dec, NULL);
 pthread_join(inc, NULL);
 pthread_join(dec, NULL);
 printf("%d\n", c);
 return 0;
}
