#include<pthread.h>
#include<semaphore.h>
#include<stdio.h>
void *func1();
void *fund2();
int shared=1;
sem_t s;
int main(int argc, char const *argv[])
{
	/* code */
	sem_init(&s,0,1);
	pthread_t thread1,thread2;
	pthread_create(&thread1,NULL,func1,NULL);
	pthread_create(&thread2,NULL,fund2,NULL);
	pthread_join(&thread1,NULL);
	pthread_join(&thread2,NULL);
	printf("Final value of shared is %d\n",shared);
	return 0;
}

void *func1()
{
	int x;
	sem_wait(&s);
	x = shared;
	x++;
	sleep(1);
	shared = x;
	sem_post(&s);
}

void *fund2()
{
	int y;
	sem_wait(&s);
	y = shared;
	y--;
	sleep(1);
	shared = y;
	sem_post(&s);
}
