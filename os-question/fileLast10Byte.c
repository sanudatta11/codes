// C program to illustrate
// read system Call
#include<stdio.h>
#include <fcntl.h>
int main()
{
  int fd,fd2, sz,ret_out;
  char *c = (char *) calloc(100, sizeof(char));
 
  fd = open("file1.txt", O_RDONLY);
  fd2 = open("file2.txt",O_WRONLY | O_CREAT | O_TRUNC,0644);
  if (fd < 0) { perror("r1"); exit(1); }
 
  sz = read(fd, c, 10);
  if(sz){
            ret_out = write (fd2, c, (ssize_t) sz);
            if(ret_out <0){
                /* Write error */
                perror("write");
                return 4;
            }
    }
  printf("called read(% d, c, 10).  returned that"
         " %d bytes  were read.\n", fd, sz);
  c[sz] = '\0';
  printf("Those bytes are as follows: % s\n", c);
}