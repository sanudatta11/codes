#include<stdio.h>
#include<stdlib.h>
#include<semaphore.h>
#include <unistd.h>
#include <pthread.h>
#include<string.h>
void *add1(){
	printf("Entering add fun\n");
	int a = 10,b=15;
	return a+b;
}
// void *prod1(){
// 	int x=10,y=2;
// 	return x*y;
// }
void main()
{
	/* code */

	pthread_t t1,t2;
	int t;
	void *status1,*status2;
	pthread_create(&t1,NULL,add1,NULL);
	// pthread_create(&t2,NULL,prod1,NULL);
	t = pthread_join(t1,&status1);
	printf("%d\n", t);
	// pthread_join(t2,&status2);
	printf("%s\n", status1);
	// printf("%s\n", status2);
}