#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#define MAX 100
int main(int argc, char const *argv[])
{
	/* code */
	int fd[2],n;
	char buffer[100];
	pid_t p;
	if(pipe(fd) == -1) {
		printf("Pipe Creation Failed\n");
	}
	p = fork();
	if(p > 0){
		//Parent
		close(fd[0]);
		printf("Passing value to child\n");
		write(fd[1],"Hello Child this is your parent\n",sizeof("Hello Child this is your parent\n"));
		close(fd[1]);
	}
	else{
		//Child
		close(fd[1]);
		printf("Reading data from parent\n");
		n = read(fd[0],buffer,100);
		write(1,buffer,n);
	}
	return 0;
}